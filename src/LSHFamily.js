
var logging = false;

if (logging === false) {
	console.log = console.trace = function() {};
}


function randomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomVector(min, max, dim) {
	var vec = [];

	for (var i = 0; i < dim; i++) {
		vec.push(randomInt(min, max));
	}

	return vec;
}

function generateUnitNormalVector(d) {
	var vec = [];
	for (var i = 0; i < d; i++) {
		vec.push((Math.random() * 2) - 1);
	};

	return normalizeVector(vec);
};

function normalizeVector(vec) {
	var d = vec.length;
	var norm = getNorm(vec);
	vec = vec.map(function(element) {
		return element / norm;
	});
	return vec;
}

function getNorm(vec) {
	var sum = vec.reduce(function(pref, curr) {
		return pref + (curr * curr);
	}, 0);
	sum = Math.sqrt(sum);
	return sum;
};

function vectorMult(a, b) {
	return a.reduce(function(prev, curr, i) {
		return prev + curr * b[i];
	}, 0);
}

function sign(n) {
	if (n > 0) {
		return 1;
	}
	return 0;
}

function _getCombinations() {

	var combinations = [];
	var m = this.m;
	var L = this.L;

	var count = 0;

	for (var i = 0; i < m; i++) {
		var j = i + 1;
		while (j < m) {

			if (count === L) {
				return combinations;
			}

			combinations.push([i, j]);
			j++;
			count++;
		}
	}
	return combinations;
}


/**
 * Hashfunctions from the family G of hash functions
 * @param {[type]} k [description]
 * @param {[type]} d [description]
 */
function GHashFunction(k, d, vectors) {
	this.vectors = vectors || [];

	if (this.vectors.length === 0) {
		// Generate Hash Functions
		for (var i = 0; i < k; i++) {
			this.vectors.push(generateUnitNormalVector(d));
		}
	}

}

GHashFunction.prototype.hash = function(vector) {
	return this.vectors.map(function(normVector) {
		return sign(vectorMult(vector, normVector));
	}).join('');
};

/**
 * [LSHFamily description]
 * @param {number} d Number
 * @param {number} k Number
 * @param {number} m Number
 */
function LSHFamily(d, k, m, L, hashFunctions) {

	this.d = d;
	this.k = k
	this.m = m;
	this.L = L;
	this.k_half = k/2;
	this.combinations = _getCombinations.call(this);

	this.hashFunctions = hashFunctions || [];

	if (this.hashFunctions.length === 0) {
		for (var i = 0; i < m; i++) {
			this.hashFunctions.push(new GHashFunction(this.k_half, this.d));
		}
	}
}

LSHFamily.prototype.getHashes = function(vector) {

	if (!Array.isArray(vector) || vector.length < this.d) {
		throw new Error("Invalid vector " +  vector + ' ' + this.d);
	}

	var results = this.hashFunctions.map(function(fn) {
		return fn.hash(vector);
	});


	return this.combinations.map(function(combination, index) {
		if (combination[0] === 'undefined') {
			console.log('undefined for com[0] at index', index);
		}
		if (combination[1] === 'undefined') {
			console.log('undefined for com[1] at index', index);
		}
		return '' + results[combination[0]] + results[combination[1]];
	});
}

LSHFamily.prototype.serialize = function() {
	return JSON.stringify(this);
};

LSHFamily.deserialize = function(data) {
	if (typeof data === 'string') {
		data = JSON.parse(data);
	}
	var k_half = this.k / 2;

	var hashFunctions = data.hashFunctions.map(function(hashData) {
		return new GHashFunction(data.d, k_half, hashData.vectors);
	});

	return new LSHFamily(data.d, data.k, data.m, data.L, hashFunctions);
};

module.exports = LSHFamily;