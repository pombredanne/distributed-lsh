var util = require('webrtc-kademlia').util;

function generate(n) {
	var ids = [];

	for (var i = 0; i < 500; i++) {
		ids.push(util.getRandomKademliaID());
	}

	return ids;
}

module.exports = generate;

/**
 * var string = JSON.stringify(generate(100))
 * fs.writeFileSync('../data/ids.json', string, 'utf-8');
	console.log('Done writing');

 */