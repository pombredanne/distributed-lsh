var LSHFamily = require('./LSHFamily');

function generate(config) {

	var _getMForL = function(l) {
		var m = 1;
		while ((m * (m - 1)) / 2 < l) {
			m++;
		}
		return m;
	}


	var d = config.d;
	var k = config.k;
	var L = config.L;
	var m = _getMForL(L);



	var family = new LSHFamily(d, k, m, L);

	return family.serialize();

}

module.exports = generate;