function trueNeighbours(test, data, n) {
	function dist(a, b) {
		var sum = 0;
		for (var i = 0; i < a.length; i++) {
			var x = a[i] - b[i];
			sum += x * x;
		}
		return Math.sqrt(sum);
	}

	var now = Date.now();
	var truu = test.map(function(vec) {
		var dists = data.map(function(date) {
			return dist(vec.vector, date.vector);
		});
		dists.sort(function(a, b) {
			return a - b;
		});
		return dists.slice(0, n);
	});

	console.log(truu.length, truu[0].length);

	return truu;

}

module.exports = trueNeighbours;
