

function quality(aprox, dists) {

	dists.forEach(function(vec, i) {
		var dist = vec[vec.length - 1];
		var j = 0;
		// console.log(aprox[i][j], dist);
		while (aprox[i][j] <= dist && j < aprox[i].length) {
			j++;
		}
		var quotient = (j / aprox[i].length) * 100;
		// console.log((j + 1) + ' of ' + aprox[i].length + ' of the aprox lie in the true top 50.');
		// var recognitionRate = Math.round(((j + 1) / 50) * 100);


		console.log('Vec ' + i + ': ' + quotient + '% of the ' + aprox.length + ' vectors lie in the first 50 knn');

	});

}


module.exports = quality;