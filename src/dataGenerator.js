
function randomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomVector(min, max, dim) {
	var vec = [];

	for (var i = 0; i < dim; i++) {
		vec.push(randomInt(min, max));
	}

	return vec;
}

function generate(config, labelCB, n) {
	var vectors = [];

	var min = config.min;
	var max = config.max;
	var dim = config.d;
	n = n || config.n;

	for (var i = 0; i < n; i++) {
		var label = labelCB(i) || 0;
		vectors.push({
			vector: randomVector(min, max, dim),
			label: label
		});
	}

	return vectors;
}

module.exports = generate;


// for (var i = 0; i < 10; i++) {
// 	generate(0, 9, d, n, '../data/d' + d + 'n' + n + '-' + i + '.json');
// }

