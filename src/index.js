var Kademlia = require('webrtc-kademlia');
var LSHFamily = require('./LSHFamily');
var jsonfile = require('jsonfile');

var Queue = require('concurrent-queue2');

var util = Kademlia.util;
var constants = util.constants;
var Q = require('q');
var _ = require('lodash');

var logging = false;

var monsole = {};
if (logging === false) {
	monsole.log = monsole.trace = function() {};
} else {
	monsole.log = console.log.bind(console);
	monsole.trace = console.trace.bind(console);
}


var PROBE_SIZE = 50;

var RPCS = {
	LSH_SEARCH_REQ: 1,
	LSH_SEARCH_RES: 2
};

var SETTLED_BULK_INSERTS = 0;

// length of the possible partitions in bit
var PARTITION_LENGTH = 32;

var PENDING_BUCKETS = [];
var NOT_CALLED_BUCKETS = [];

for (var i = 0; i < 127; i++) {
	PENDING_BUCKETS.push(i + 1);
	NOT_CALLED_BUCKETS.push(i + 1);
}




/**
 * Returns the key, where the hash functions should be
 * @return {String} 192 bit Address Key
 */
var _getHashAddress = function() {
	var binAppID = util.b64ToBinary(this.appID);
	for (var i = 0; i < constants.HASH_SPACE; i++) {
		binAppID += '0';
	}
	return binAppID;
}

var _getMForL = function(l) {
	var m = 1;
	while ((m * (m - 1)) / 2 < l) {
		m++;
	}
	return m;
}

// var _initFamily = function() {
// 	this.m = _getMForL(this.L);

// 	// generate family
// 	this.family = new LSHFamily(this.d, this.k, this.m, this.L);

// 	// and propagate it to the network
// 	var json = _serializeFamily.call(this);

// 	return this.kademlia.NODE_LOOKUP(this.familyAddress)
// 	.then(function success(peers) {

// 		var promises = peers.map(function(peer) {
// 			return this.kademlia.STORE(peer, this.familyAddress, json);
// 		}, this);

// 		return Q.allSettled(promises);

// 	}.bind(this), function fail(err) {
// 		console.trace('err', err);
// 	});

// };

var _getBitsForL = function(l) {

	var n = this.n;

	var bin = l.toString(2);

	if (bin.length === n) {
		return bin;
	}

	var zeroesMissing = n - bin.length;

	for (var i = 0; i < zeroesMissing; i++) {
		bin = '0' + bin;
	}

	return bin;

}


/**
 * General Key Generation Function. In the first implementation it ignores the partition.
 * Although, it's the second parameter, because it's also at the second location in the key
 * @param  {Number} l         The Bucket ID l
 * @param  {String} partition The partition binary string.
 * @param  {String} hash      The LSH Hash of the vector
 * @return {String}           The Key
 */
var _getKey = function(l, partition, hash) {

	l = l || null;
	partition = partition || null;
	hash = hash || null;

	if (l === null) {
		throw new Error("l is needed for the key generation");
	}

	var key = '';
	key += util.b64ToBinary(this.appID);
	key += _getBitsForL.call(this, l);

	if (hash === null) {
		var bitsMissing = constants.COMPLETE_LENGTH - key.length;

		for (var i = 0; i < bitsMissing; i++) {
			key = key + '0';
		}

		return key;
	}

	/**
	 * 1. Add Partition bits
	 */

	for (var i = 0; i < PARTITION_LENGTH; i++) {
		key = key + '0';
	}

	/**
	 * 2. Add the LSH Hash
	 */

	// hash should come in binary format
	key = key + hash;


	/**
	 * 3. Fill the missing bits with zeroes
	 */

	var bitsMissing = constants.COMPLETE_LENGTH - key.length;


	for (var i = 0; i < bitsMissing; i++) {
		key = key + '0';
	}

	return key;

};

var _getLFromKey = function(key) {
	return 0;
}

var _getHashFromKey = function(key) {
	return '';
}

var _getHashes = function(vector) {
	var hashes = this.family.getHashes(vector);

	return hashes.map(function(hash, index) {

		// l starts with 1
		var l = index + 1;

		return util.binaryToB64(_getKey.call(this, l, null, hash));

	}, this);

}

var _hashIntoBuckets = function(data) {
	var buckets = [];

	// init buckets with empty values

	for (var i = 0; i < this.L; i++) {
		buckets.push({});
	}


	data.forEach(function(date) {
		var vector = date.vector;

		var hashes = this.family.getHashes(vector);

		hashes.forEach(function(hash, index) {

			var bucket = buckets[index];

			// l starts with 1
			var l = index + 1;

			var key = util.binaryToB64(_getKey.call(this, l, null, hash));

			if (!bucket[key]) {
				bucket[key] = [];
			}

			bucket[key].push(date);

		}, this);

	}, this);

	return buckets;
}

/**
 * Save the content of the complete bucket to the target
 * @param  {Number} bucketID Value between 1...L
 * @param  {Object} bucket   {'hash': [vectors...]}
 * @return {Promise}
 */
var _saveBucket = function(bucketID, bucket) {

	// console.trace('Calling _saveBucket with', bucketID, bucket);

	var iii = NOT_CALLED_BUCKETS.indexOf(bucketID);

	NOT_CALLED_BUCKETS.splice(iii, 1);

	return new Q.Promise(function(resolve, reject) {

		// 1. Generate Lookup Address

		var address = util.binaryToB64(_getKey.call(this, bucketID));

		// 2. Get K peers, that are interesting for our ids
		console.log('pending for l=', bucketID, ':', this.queue.NODE_LOOKUP.pending.length);

		this.queue.NODE_LOOKUP.add({
			scope: this.kademlia.requester,
			reference: this.kademlia.NODE_LOOKUP,
			args: [address]
		})
		.then(function(nodes) {
			try {

				// console.log('l=' , bucketID , '#nodes for saveBucket: ', nodes.length);

				// console.log(this.queue.BULK_STORE.items.length, this.queue.NODE_LOOKUP.items.length);

				var promises = nodes.map(function(node) {

					// before queue surgery
					// this.kademlia.BULK_STORE(node, bucket)
					// .then(function success(res) {
					// 	console.trace("YESSSS STORRED BUCKET NR. " + bucketID);
					// }, function fail() {
					// 	console.trace('Oopsy Woopsy. Couldnt store bucket nr ' + bucketID);
					// });

					// after botox surgery

					return this.queue.BULK_STORE.add({
						scope: this.kademlia.requester,
						reference: this.kademlia.requester.BULK_STORE,
						args: [node, bucket]
					});


				}, this);

				Q.allSettled(promises)
				.then(function(res) {
					// monsole.log('All BULK_STORES settled', res);
					// monsole.log('Settled BS', ++SETTLED_BULK_INSERTS, 'l=', bucketID , 'pending', this.queue.BULK_STORE.pending.length);
					var indi = PENDING_BUCKETS.indexOf(bucketID);
					PENDING_BUCKETS.splice(indi, 1);
					if (PENDING_BUCKETS.length < 10) {
						// monsole.log('PENDING:', PENDING_BUCKETS, 'NOT CALLED:', NOT_CALLED_BUCKETS);
					}

					console.log('done for l=', bucketID);

					resolve(res);
				}.bind(this), function(err) {
					monsole.log('FFAIIILLLLL');
					reject(err);
				});

			} catch (e) {
				console.trace(e);
			}

		}.bind(this), function (err) {
			console.trace('err', err);
		});

	}.bind(this));

};

var _searchBucket = function(bucketID, hash, date) {


	// 1. Generate Lookup Address
	var address = util.binaryToB64(_getKey.call(this, bucketID));

	// 2. Get K peers, that are interesting for our ids
	return new Q.Promise(function(resolve, reject) {

		this.queue.NODE_LOOKUP.add({
			scope: this.kademlia.requester,
			reference: this.kademlia.NODE_LOOKUP,
			args: [address]
		})
		.then(function(nodes) {

			try {

				var nodesLeft = nodes.slice();

				function getCollisions(node) {

					// monsole.log('calling getCollisions for', node);

					this.kademlia.transport
					.send(node)
					.payload({
						rpc: RPCS.LSH_SEARCH_REQ,
						l: bucketID,
						hash: hash,
						date: date
					})
					.then(function success(res) {
						// monsole.log('lsh search answer', res.vectors && res.vectors.length);
						resolve(res);
					}, function fail(err) {
						monsole.log('faiiiil', err);
						reject(err);
						return;
						if (nodesLeft.length > 0) {
							getCollisions.call(this, nodesLeft.shift());
						} else {
							reject('Nobody has my data for l=', bucketID);
						}
					});

				}

				getCollisions.call(this, nodesLeft.shift());


			} catch (e) {
				reject(e);
				console.trace(e);
			}

		}.bind(this), function (err) {
			console.trace('err', err);
		});

	}.bind(this));


};

var _processor = function(task) {

	return new Promise(function(resolve, reject) {

		function functionName(fun) {
		  var ret = fun.toString();
		  ret = ret.substr('function '.length);
		  ret = ret.substr(0, ret.indexOf('('));
		  return ret;
		}

		// monsole.log('Processing Task', functionName(task.reference), task.args);

		var scope = task.scope;

		var fn = task.reference;
		fn = typeof fn === 'function' ? fn : null;

		var args = task.args;
		args = Array.isArray(args) ? args : [];


		fn.apply(scope, args)
			.then(function success(res) {
				resolve(res);
			}, function fail(err) {
				reject(err);
			});

	});

}

function DistributedLSH (options) {

	this.k             = options.k;
	this.L             = options.L;
	this.n             = Math.round(Math.log2(this.L + 1));

	if ((Math.pow(2, this.n) - 1) !== this.L) {
		throw new Error("L must be 2^n - 1. " + this.n);
	}

	this.m                   = null; // we get m later
	this.d                   = options.d;
	this.appID               = options.appID || util.getRandomAppID();
	this.threshold           = options.threshold;
	this.myID                = options.myID || util.getRandomKademliaID();
	this.trickle             = typeof options.trickle === 'boolean' ? options.trickle : true;
	this.kademlia            = new Kademlia(this.appID, this.myID, this.trickle);

	this.familyPath          = options.familyPath || __dirname + '/../data/family.json';
	this.family              = LSHFamily.deserialize(jsonfile.readFileSync(this.familyPath));
	this.binaryFamilyAddress = _getHashAddress.call(this);
	this.familyAddress       = util.binaryToB64(this.binaryFamilyAddress);

	this.kademlia.transport.on('request', function(req) {
		this.handleRequest(req);
	}.bind(this));


	/**
	 * seperate concurrency queues for each RPC, because we could get a deadlock
	 * with the following calls:
	 * concurrency_factor = 3
	 * queue.add(LOOKUP1)
	 * queue.add(LOOKUP2)
	 * queue.add(LOOKUP3)
	 * -> queue = [LOOKUP1, LOOKUP2, LOOKUP3]
	 * But maybe the LOOKUPi's are now queing
	 * queue.add(FIND_NODE1)
	 * queue.add(FIND_NODE2)
	 * queue.add(FIND_NODE3)
	 * Which will never happen, because the 3 lookup tasks are already open.
	 * With seperate queues for each rpc we can avoid a lot of problems!
	 */

	this.queue = {
		PING: null,
		VALUE_LOOKUP: null,
		NODE_LOOKUP: null,
		FIND_VALUE: null,
		FIND_NODE: null,
		STORE: null,
		BULK_STORE: null
	};

	Object.keys(this.queue).forEach(function(key) {

		this.queue[key] = new Queue(_processor, constants.CONCURRENCY_FACTOR);

	}, this);


}


DistributedLSH.randomID = function() {
	return util.getAppID();
}


function _getRPC(n) {

	var rpcs = Object.keys(RPCS);

	var rpc = rpcs.filter(function(key) {
		var value = RPCS[key];
		return value === n;
	});

	return rpc.length > 0 ? rpc[0] : null;

}

DistributedLSH.prototype = {
	init: function() {
		return this.kademlia.join();

			// .then(function success(res) {
				// this.kademlia.VALUE_LOOKUP(this.familyAddress)
				// 	.then(function success() {
				// 		console.trace('There are already some hashes. ', hashes);

				// 		var data = res.data;
				// 		var hashes = data.value;

				// 		if (hashes) {
				// 			this.family = _deserializeFamily.call(this, hashes);
				// 		} else {
				// 			return _initFamily.call(this);
				// 		}

				// 	}.bind(this), function error(err) {
				// 		return _initFamily.call(this);
				// 	}.bind(this));


				// }.bind(this), function error(err) {
				// 	console.trace('join failed', err);
				// });

	},

	handleRequest: function(req) {
		var data = req.data || null;
		if (!data) {
			console.trace('Got request, but without data. ', req);
			return;
		}

		var rpc = _getRPC(data.rpc);

		// monsole.log('got req.', data);

		if (rpc !== null) {
			this[rpc](req);
		}
	},

	add: function(data) {

		data = Array.isArray(data) ? data : [data];

		var buckets = _hashIntoBuckets.call(this, data);

		var promises = buckets.map(function(bucket, index) {

			/**
			 * Bucket IDs are in the room 1...L
			 */

			return _saveBucket.call(this, index + 1, bucket);

		}, this);


		return Q.allSettled(promises);

	},

	search: function(date, max) {

		return new Q.Promise(function(resolve, reject) {

			var vector = date.vector;

			var hashes = _getHashes.call(this, vector);

			var max = max && typeof max === 'number' ? max : 50;

			// monsole.log('hashes', hashes);

			var PROMISE_COUNT = 0;

			promises = hashes.filter(function(a, index) {
				return index < PROBE_SIZE;
			})
			.map(function(hash, index) {

				/**
				 * Bucket IDs are in the room 1...L
				 */

				// SEARCH

				return _searchBucket.call(this, index + 1, hash, date);


			}, this);

			var called = false;





			var results = [];

			// promises.forEach(function(promise) {
			// 	promise.then(function(res) {
			// 		PROMISE_COUNT++;
			// 		results.push(res);
			// 		if (PROMISE_COUNT > 120) {
			// 			doney.call(this, results);
			// 		}
			// 	}.bind(this), function(err) {
			// 		PROMISE_COUNT++;

			// 	});
			// }.bind(this));


			Q.allSettled(promises)
			.then(doney.bind(this), function fail(err) {
				console.trace('error', err);
				reject(err);
			});

			function doney(responses) {

				// debugger
				try {


					var valid = responses.map(function(res) {
						return res.value || {};
					}).filter(function(res) {
						return res.hasOwnProperty('vectors') && res.vectors.length > 0;
					})
					.map(function(res) {
						return res.vectors;
					});

					// monsole.log('valid', valid);

					// monsole.log('got ', valid.length, 'valid responses');

					var merged = [];

					merged = merged.concat.apply(merged, valid);

					merged = _.uniq(merged, function(date) {
						return date.vector.join('|');
					});


					merged = merged.sort(function(a, b) {
						var dist1 = util.eucledianDistance(a.vector, vector);
						var dist2 = util.eucledianDistance(b.vector, vector);

						return dist1 - dist2;
					});

					merged = merged.slice(0, max);

					resolve(merged);
				} catch (e) {
					console.log(e);
				}
			}


		}.bind(this));

	},

	LSH_SEARCH_REQ: function(req) {
		var peer = req.peerID || null;
		var data = req.data || null;
		var l = data.l || null;
		var hash = data.hash || null;
		var date = data.date || null;
		var vector = date.vector || null;
		var limit = data.limit || 50;


		if (l === null || hash === null || date === null || vector === null) {

			req.respond()
			.payload({
				rpc: RPCS.LSH_SEARCH_RES,
				error: "Can't respond. l, hash or date is missing"
			})
			.then(function success(res) {
				// log
			}, function fail() {
				// log
			});

			throw new Error("Can't respond. l, hash or date is missing", !!l, !!hash, !!date);

		}

		this.kademlia.storage.get(hash)
		.then(function success(value) {
			if (value) {
				// monsole.log('Got value at LSH_SEARCH:', value);
				var nearest = value.sort(function(a, b) {
					var dist1 = util.eucledianDistance(a.vector, vector);
					var dist2 = util.eucledianDistance(b.vector, vector);

					return dist1 - dist2;
				});

				var of = nearest.length;
				nearest = nearest.slice(0, limit);

				req.respond()
				.payload({
					rpc: RPCS.LSH_SEARCH_RES,
					vectors: nearest,
					from: nearest.length
				})
				.then(function success() {
					// log
				}, function fail() {
					// log
				});

			} else {
				req.respond()
				.payload({
					rpc: RPCS.LSH_SEARCH_RES,
					vectors: []
				})
				.then(function success() {
					// log
				}, function fail() {
					// log
				});
			}
		}, function fail(err) {
			req.respond()
			.payload({
				rpc: RPCS.LSH_SEARCH_RES,
				error: "Sorry, my storage failed :/"
			})
			.then(function success(res) {
				// log
			}, function fail() {
				// log
			});

			throw new Error('Storage failed at LSH_SEARCH_REQ answering');
		});

	}



};

module.exports = DistributedLSH;
