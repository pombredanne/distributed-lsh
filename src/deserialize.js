var fs = require('fs');
var familyData = require('../data/family.json');
var data = require('../data/d100n3.json');
var LSHFamily = require('./LSHFamily');


var family = LSHFamily.deserialize(familyData);

// console.log(data[0]);

var hashes = family.getHashes(data[0].vector);
console.log(hashes);