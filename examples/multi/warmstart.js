var DistributedLSH = require('../../src/index');
var test = require('../../data/d1000n3.json');
var realNeighbours = require('../../data/neighbours1000.json');
var config = require('./config');
var quality = require('../../src/quality');

var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000
});

dlsh.init()
.then(function() {

		console.log('Starting to search...');
		var beforeSearch = Date.now();

		dlsh.search(test[0])
		.then(function(res) {


			var aprox = res.map(function(date) {
				return date.vector;
			});

			console.log('SEARCHING DONE. Got ', res.length, 'vectors.');
			console.log('It took' + (Date.now() - beforeSearch) + 'ms');

			quality(aprox, realNeighbours);


		}, function(err) {
			console.trace('SEARCHING FAILED', err, err.stack);
		});


}, function() {
	console.log("Couldn't init dlsh");
});
