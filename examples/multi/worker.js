var DistributedLSH = require('../../src/index');
var config = require('./config');
var memwatch = require('memwatch-next');
var ids = require('./ids.json');
var fs = require('fs');

var n = -1;

var interval = 150 * 1000; // all 500 seconds

var hd = new memwatch.HeapDiff();

var leaks = [];

memwatch.on('leak', function(info) {
	leaks.push(info);
});

setInterval(function() {

	var diff = hd.end();
	
	fs.writeFileSync('./diffs/worker-' + n + '-' + Date.now() + '.json', JSON.stringify(diff));

	hd = new memwatch.HeapDiff();


	fs.writeFileSync('./diffs/worker-leaks-' + n + '-' + Date.now() + '.json', JSON.stringify(leaks));

	leaks = [];

}, interval);


process.on('message', function(msg) {
	n = parseInt(msg.content, 10);
	var id = ids[n];


	var dlsh = new DistributedLSH({
		k: config.k,
		L: config.L,
		d: config.d,
		appID: 'BktLLD',
		threshold: 20000
	});

	dlsh.init()
	.then(function(res) {
		console.log('I am worker ' + id.substr(id.length - 2)  + ' :)');
	}, function(err) {
		err = err.hasOwnProperty('stack') ? err.stack : err;
		console.log('fail', err);
	});


	setTimeout(function() {



	}, 160000);

});
