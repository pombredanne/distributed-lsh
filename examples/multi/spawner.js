var child_process = require('child_process');


for (var i = 1; i < 10; i++) {
	var child = child_process.fork(__dirname + '/worker', [i], {
		silent: false
	});

	child.send({
		msgtype: 'n',
		content: i
	});
}
