var DistributedLSH = require('../../src/index');
var n = process.argv[2];


// var test = require('../data/d100n3.json');
var config = require('./config');
var ids = require('../../data/ids.json');
var fs = require('fs');


var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000,
	myID: ids[0]
});

var beforeJoin = Date.now();

var indices = [];

for (var i = 0; i < 10; i++) {
	indices.push(i);
}

dlsh.init()
.then(function() {

	console.log('Joined network :), it took ' + (Date.now() - beforeJoin) +  'ms');


	function add(index) {

		var beforeAdd = Date.now();
		console.log("Calling dlsh.add() with i=" + index);

		var data = JSON.parse(fs.readFileSync('../../data/d1000n1000-' + index + '.json'));

		dlsh.add(data).then(function(res) {

			console.log('YEEHEHEEHE. Data took ');

			console.log((Date.now() - beforeAdd) + ' ms');

			if (indices.length > 0) {
				add(indices.shift());
			}

		}, function(err) {

			err = err.hasOwnProperty('stack') ? err.stack : err;
			console.log('fail', err);

		});

	}


	try {
		

	if (indices.length > 0) {
		add(indices.shift());
	}

	} catch (e) { console.log(e.stack); }

}, function() {
	console.log("Couldn't init dlsh");
});
