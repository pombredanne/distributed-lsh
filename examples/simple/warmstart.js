var DistributedLSH = require('../src/index');
var test = require('../data/d100n3.json');
var realNeighbours = require('../data/neighbours.json');
var config = require('./config');
var quality = require('../src/quality');

var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000
});

dlsh.init()
.then(function() {

		console.log('Starting to search...');

		dlsh.search(test[0])
		.then(function(res) {


			var aprox = res.map(function(date) {
				return date.vector;
			});

			console.log('SEARCHING DONE. Got ', res.length, 'vectors.');

			quality(aprox, realNeighbours);


		}, function(err) {
			console.trace('SEARCHING FAILED', err, err.stack);
		});


}, function() {
	console.log("Couldn't init dlsh");
});