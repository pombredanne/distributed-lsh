var DistributedLSH = require('../../src/index');
var config = require('./config');
var ids = require('../../data/ids.json');

var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000,
	myID: ids[1]
});

dlsh.init()
.then(function(res) {
	console.log('Im a worker :)');
}, function(err) {
	err = err.hasOwnProperty('stack') ? err.stack : err;
	console.log('fail', err);
});