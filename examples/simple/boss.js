var DistributedLSH = require('../../src/index');
var data = require('../../data/d100n100.json');
// var test = require('../../data/d100n3.json');
var config = require('./config');
var ids = require('../evaluation/ids.json');


var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000,
	myID: ids[0]
});

dlsh.init()
.then(function() {
	console.log('Joined network :)');
	dlsh.add(data).then(function(res) {

		console.log('YEEHEHEEHE');

	}, function(err) {

		err = err.hasOwnProperty('stack') ? err.stack : err;
		console.log('fail', err);

	});

}, function() {
	console.log("Couldn't init dlsh");
});