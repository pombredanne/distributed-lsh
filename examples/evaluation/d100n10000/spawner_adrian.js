var child_process = require('child_process');


for (var i = 10; i < 15; i++) {
	var child = child_process.fork(__dirname + '/worker', [i], {
		silent: false
	});

	child.send({
		msgtype: 'n',
		content: i
	});
}
