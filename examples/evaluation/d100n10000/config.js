module.exports = {
	k: 30,
	L: 127,
	d: 100,
	min: 0,
	max: 9,
	labels: ['a', 'b'],
	n: 5000,
	i: 2,
	test: 3
};