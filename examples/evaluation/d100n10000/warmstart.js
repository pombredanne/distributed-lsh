var DistributedLSH = require('../../../src/index');
var test = require('./d100n3.json');
var realNeighbours = require('./neighbours.json');
var config = require('./config');
var quality = require('../../../src/quality');

var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000,
	familyPath: './family.json'
});

dlsh.init()
.then(function() {
		var peers = dlsh.kademlia.routingTable.buckets;

		// console.log('Joined the network. Now knowing ', peers);
		console.log('Starting to search...');
		var beforeSearch = Date.now();

		// test.forEach(function(date, index) {

			dlsh.search(test[0])
			.then(function(res) {

				console.log('WHOOT');

				try {

					var aprox = res.map(function(date) {
						return date.vector;
					});

					console.log('SEARCHING DONE. Got ', res.length, 'vectors.');
					console.log('It took ' + (Date.now() - beforeSearch) + 'ms');

					quality(aprox, realNeighbours);


				} catch (e) {
					console.log(e.trace);
				}

			}, function(err) {
				console.trace('SEARCHING FAILED', err, err.stack);
			});

		// });



}, function() {
	console.log("Couldn't init dlsh");
});
