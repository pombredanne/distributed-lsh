var dataGenerator = require('../../../src/dataGenerator');
var familyGenerator = require('../../../src/familyGenerator');
var idGenerator = require('../../../src/idGenerator');
var trueNeighbours = require('../../../src/trueneighbours');

var config = require('./config');
var jsonfile = require('jsonfile');
var fs = require('fs');

/**
 * 1. Generate the Data
 */

var datass = [];

for (var i = 0; i < config.i; i++) {
	console.log('Generating Data with i=' + i + ' d=' + config.d + ', n=' + config.n);
	var data = dataGenerator(config, function(i) {
		return config.labels[i % 2]
	});

	jsonfile.writeFile('i' + i + 'd' + config.d + 'n' + config.n + '.json', data, function(err) {
		if (err) console.log(err);
		console.log('Saved Data to disk.');

	});

	datass = datass.concat(data);
}





/**
 * 2. Generate Test Data
 */

console.log('Generating Test Data with d=' + config.d + ', n=' + config.test);
var test = dataGenerator(config, function(i) {
	return config.labels[i % 2]
}, config.test);

jsonfile.writeFile('d' + config.d + 'n' + config.test + '.json', test, function(err) {
	if (err) console.log(err);
	console.log('Saved Data to disk.');

});

/**
 * 3. Calculate the true neighbours
 */
console.log('Generating the true neighbours');
var before = Date.now();
var neighbours = trueNeighbours(test, datass, 50);
console.log('Generating those took ' + (Date.now() - before) + 'ms');
jsonfile.writeFile('neighbours.json', neighbours, function(err) {
	if (err) console.log(err);
	console.log('Saved Data to disk.');

});



/**
 * 3. Generate the corresponding LSH Family
 */

console.log('Generating Family');
var family = familyGenerator(config);
// it's already stringified json
fs.writeFile('family.json', family, 'utf-8', function(err) {
	if (err) console.log(err);
	console.log('Saved Family to disk.');
	family = null;
});

/**
 * 4. Generate ids
 */
console.log('Generating ids');
var ids = idGenerator(30);
jsonfile.writeFile('ids.json', ids, function(err) {
	if (err) console.log(err);
	console.log('Done generating ids');
	ids = null;

	// needs to be done because we require Kademlia in one Generator
	process.exit(0);
});
