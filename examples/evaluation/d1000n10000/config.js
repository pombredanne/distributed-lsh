module.exports = {
	k: 30,
	L: 127,
	d: 1000,
	min: 0,
	max: 9,
	labels: ['a', 'b'],
	n: 500,
	i: 20,
	test: 3
};