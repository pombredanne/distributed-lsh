var DistributedLSH = require('../../../src/index');
var jsonfile = require('jsonfile');
var config = require('./config');
var data = jsonfile.readFileSync('./d10n100.json');
var ids = require('./ids.json');


var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000,
	myID: ids[25],
	familyPath: './family.json'
});

var beforeJoin = Date.now();

dlsh.init()
.then(function() {

	console.log('Joined network :), it took ' + (Date.now() - beforeJoin) +  'ms');

	try {

	var beforeAdd = Date.now();
	console.log('Now calling dlsh.add() with d=' + config.d + ', n=' + data.length);

	dlsh.add(data).then(function(res) {

		console.log('YEEHEHEEHE. Data took ');

		console.log((Date.now() - beforeAdd) + ' ms');

	}, function(err) {

		err = err.hasOwnProperty('stack') ? err.stack : err;
		console.log('fail', err);

	});

	} catch (e) { console.log(e.stack); }

}, function() {
	console.log("Couldn't init dlsh");
});
