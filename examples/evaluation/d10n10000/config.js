module.exports = {
	k: 10,
	L: 127,
	d: 10,
	min: 0,
	max: 9,
	labels: ['a', 'b'],
	n: 100,
	test: 3
};