var mnist = require('mnist');
var familyGenerator = require('../../../src/familyGenerator');
var idGenerator = require('../../../src/idGenerator');
var trueNeighbours = require('../../../src/trueneighbours');

var config = require('./config');
var jsonfile = require('jsonfile');
var fs = require('fs');

/**
 * 1. Generate the Data
 */

var datass = [];

console.log('Generating mnist Data with (' + config.n + ', ' + config.test + ')');

var set = mnist.set(config.n, config.test);

var trainingSet = set.training;
var testSet = set.test;

trainingSet = trainingSet.map(function(date) {
	return {
		vector: date.input,
		label: date.output
	}
});

testSet = testSet.map(function(date) {
	return {
		vector: date.input,
		label: date.output
	}
});

var sliceLength = trainingSet.length / config.i;

for (var i = 0; i < config.i; i++) {
	var data = trainingSet.slice(sliceLength * i, sliceLength * (i + 1));

	jsonfile.writeFile('i' + i + 'd' + config.d + 'n' + config.n + '.json', data, function(err) {
		if (err) console.log(err);
		console.log('Saved Data to disk.');

	});

}





/**
 * 2. Generate Test Data
 */

console.log('Generating Test Data with d=' + config.d + ', n=' + config.test);

jsonfile.writeFile('d' + config.d + 'n' + config.test + '.json', testSet, function(err) {
	if (err) console.log(err);
	console.log('Saved Data to disk.');

});

/**
 * 3. Calculate the true neighbours
 */
console.log('Generating the true neighbours');
var before = Date.now();
var neighbours = trueNeighbours(testSet, trainingSet, 50);
console.log('Generating those took ' + (Date.now() - before) + 'ms');
jsonfile.writeFile('neighbours.json', neighbours, function(err) {
	if (err) console.log(err);
	console.log('Saved Data to disk.');

});



/**
 * 3. Generate the corresponding LSH Family
 */

console.log('Generating Family');
var family = familyGenerator(config);
// it's already stringified json
fs.writeFile('family.json', family, 'utf-8', function(err) {
	if (err) console.log(err);
	console.log('Saved Family to disk.');
	family = null;
});

/**
 * 4. Generate ids
 */
console.log('Generating ids');
var ids = idGenerator(30);
jsonfile.writeFile('ids.json', ids, function(err) {
	if (err) console.log(err);
	console.log('Done generating ids');
	ids = null;

	// needs to be done because we require Kademlia in one Generator
	process.exit(0);
});
