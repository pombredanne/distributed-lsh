var DistributedLSH = require('../../../src/index');
var config = require('./config');

var ids = require('./ids.json');

var n = -1;

process.on('message', function(msg) {
	n = parseInt(msg.content, 10);
	var id = ids[n];


	var dlsh = new DistributedLSH({
		k: config.k,
		L: config.L,
		d: config.d,
		appID: 'BktLLD',
		threshold: 20000,
		familyPath: './family.json',
		myID: id
	});

	dlsh.init()
	.then(function(res) {
		console.log('I am worker ' + id + ' :)');
	}, function(err) {
		err = err.hasOwnProperty('stack') ? err.stack : err;
		console.log('fail', err);
	});

});
