var DistributedLSH = require('../../../src/index');
var test = require('./d784n1000.json');
var realNeighbours = require('./neighbours.json');
var config = require('./config');
var jsonfile = require('jsonfile');

var dlsh = new DistributedLSH({
	k: config.k,
	L: config.L,
	d: config.d,
	appID: 'BktLLD',
	threshold: 20000,
	familyPath: './family.json'
});


var _getLabelDistribution = function(dates) {
	var labels = {};
	dates.forEach(function(date) {
		var n = _getNForLabelVector(date.label);
		if (!labels[n]) {
			labels[n] = 0;
		}
		labels[n]++;
	});
	return labels;
}

var _getNForLabelVector = function(label) {
	var i = label.indexOf(1);
	return i + 1;
}

var _getMax = function(obj) {
	var maxValue = -1;
	var maxKey = -1;
	Object.keys(obj).forEach(function(key) {
		if (obj[key] > maxValue) {
			maxValue = obj[key];
			maxKey = key;
		}
	});
	return parseInt(maxKey, 10);
}

var quality = {
	success: 0,
	fail: 0
};


dlsh.init()
.then(function() {
		var peers = dlsh.kademlia.routingTable.buckets;

		// console.log('Joined the network. Now knowing ', peers);

		// test.forEach(function(date, index) {

		function search(vec) {
		var beforeSearch = Date.now();
			dlsh.search(vec)
			.then(function(res) {

				console.log('Starting to search...');

				try {

					var aprox = res.map(function(date) {
						return date.vector;
					}).slice(0,3);

					console.log('SEARCHING DONE. Got ', res.length, 'vectors.');
					console.log('It took ' + (Date.now() - beforeSearch) + 'ms');


					// quality(aprox, realNeighbours);

					var distribution = _getLabelDistribution(res);
					var realLabel = _getNForLabelVector(vec.label);
					var predictedLabel = _getMax(distribution);
					
					console.log('predicted', predictedLabel, 'but have', realLabel);

					if (realLabel === predictedLabel) {
						quality.success++;
					} else {
						quality.fail++;
					}

					if (quality.success + quality.fail == 700) {
						jsonfile.writeFileSync('warm-results-700.json', quality);
					}
	
					console.log(quality);


					if (test.length > 0) {
						console.log('and again :)');
						setTimeout(function() {
							search(test.shift());
						}, 2000);
					}

				} catch (e) {
					console.log(e.trace);
				}

			}, function(err) {
				console.trace('SEARCHING FAILED', err, err.stack);
			});

		};


		search(test.shift());


}, function() {
	console.log("Couldn't init dlsh");
})
.then(function() {
	
}, function(err) {
	console.log('Oops.', err.stack);
});
